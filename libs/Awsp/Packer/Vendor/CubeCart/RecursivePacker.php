<?php
/**
 * This implementation first attempts to merge items into previous packages using
 * available IMergeStrategies, if any; remaining items are then packed recursively
 * in either a square or vertical stack, depending on which is more effecient.
 *
 * CubeCart item array entries are typically prefixed with 'product_', e.g. 'product_weight'.
 *
 * @package Awsp\Packer\Vendor\Cubecart
 * @author Brian Sandall
 * @copyright (c) 2016 Brian Sandall
 * @version 06/16/2016 - NOTICE: This is beta software.  Although it has been tested, there may be bugs and 
 *      there is plenty of room for improvement.  Use at your own risk.
 * @license MIT License http://www.opensource.org/licenses/mit-license.php
 */
namespace Awsp\Packer\Vendor\Cubecart;

class RecursivePacker extends \Awsp\Packer\RecursivePacker
{
    /** Weight of packaging (if any) based on store settings */
    private $packaging_weight = 0.0;

    /** Default package dimensions based on store settings */
    private $default_dimensions;

    /** Override */
    protected function init(array $init_options) {
        parent::init($init_options);
        $default = $this->getMeasurementValue(8); // set default dimensions to 8 inches and convert based on store settings
        $this->default_dimensions = array('product_width' => $default, 'product_height' => $default, 'product_depth' => $default);
    }

    /**
     * Sets the amount of weight to add to each package to account for packaging material
     * @param float $value Must also expect possible empty string in place of zero from CubeCart
     *                     Value is assumed to use the correct unit of measure
     */
    public function setPackagingWeight($value) {
        $this->packaging_weight = (empty($value) ? 0 : max(0.0, $this->getValidatedFloat($value)));
    }

    /**
     * Sets default package dimensions for cases in which a product's dimensions are not specified
     * @param array $dimensions Array containing exactly 3 float or integer values
     *                          Values are assumed to use the correct unit of measure
     */
    public function setDefaultDimensions(array $dimensions) {
        $values = filter_var(array_values($dimensions), FILTER_VALIDATE_FLOAT, FILTER_REQUIRE_ARRAY);
        if (!is_array($values) || count($values) !== 3) {
            throw new \InvalidArgumentException("Expected exactly 3 float or integer values contained in an array; received: " . implode(',', $dimensions));
        }
        rsort($values);
        $this->default_dimensions = array_combine(array_keys($this->default_dimensions), $values);
    }

    /**
     * @Override
     * Implementation that expects format as in CubeCart's AwspMultiPackageProducts plugin.
     */
    protected function prePackage(array &$items) {
        $additional = array();
        $unset = array();
        foreach ($items as $index => $item) {
            if (isset($item['additional_packages']) && is_array($item['additional_packages'])) {
                $flag = false;
                foreach ($item['additional_packages'] as $addon) {
                    $additional[] = $this->getAdditionalPackage($item, $addon);
                    $flag = true;
                }
                // Only ignore the main package if at least one additional package was found
                if ($flag && !empty($item['ignore_main_package'])) {
                    $unset[] = $index;
                }
            }
        }
        if (!empty($unset)) {
            foreach ($unset as $index) {
                unset($items[$index]);
            }
        }
        if (!empty($additional)) {
            $items = array_merge($items, $additional);
        }
    }

    /**
     * @Override
     * @param array $item An array containing 'product_weight', 'product_width', 'product_height', 'product_depth', and possibly 'quantity'
     */
    protected function getPackageWorker($item, array &$packages) {
        if (!is_array($item)) {
            throw new \InvalidArgumentException("Expected item to be an array; received " . getType($item));
        }
        // Extract required values from $item parameter
        $array = array_intersect_key($item, array('product_weight' => 0, 'product_width' => 0, 'product_height' => 0, 'product_depth' => 0));
        // Set any missing or invalid product dimensions to the store defaults
        $n = 3; // number of set dimensions
        foreach ($this->default_dimensions as $k => $v) {
            if (!array_key_exists($k, $array) || !filter_var($array[$k], FILTER_VALIDATE_FLOAT) || $array[$k] <= 0) {
                $array[$k] = (float) $v;
                $n--;
            }
        }
        // Product dimensions must either not be set at all, or all must be set
        if ($n < 3 && $n > 0) {
            throw new \InvalidArgumentException("Product dimensions must all be set or all left blank: $n dimension(s) were set");
        } elseif (count($array) < 4) {
            throw new \InvalidArgumentException("Item must contain the following fields: 'product_width', 'product_height', 'product_depth', 'product_weight', and usually 'quantity'");
        }
        extract($array);
        $quantity = $this->getQuantityFromItem($item);
        // CubeCart product weight is already per-item; packaging weight added on a per-package basis later
        
        // Validate item dimensions and sort
        $lwh = $this->getSortedDimensions($this->getValidatedFloat($product_width), $this->getValidatedFloat($product_height), $this->getValidatedFloat($product_depth));
        
        // Determine package options for single item and adjust insurance amount
        $options = $this->getPackageOptions($item);
        if ($quantity > 1 && array_key_exists('insured_amount', $options)) {
            $options['insured_amount'] /= $quantity;
        }
        
        // Ensure item is at least able to be packed individually
        $this->single_item = new \Awsp\Ship\Package($product_weight, $lwh, $options);
        if (!$this->checkConstraints($this->single_item, $error)) {
            throw new \InvalidArgumentException("Invalid package: $error");
        }
        // If item requires individual shipping, do not merge and skip recursive packing
        if (!empty($options['ships_individually'])) {
            // Adjust package weight for packaging material
            if ($this->packaging_weight > 0) {
                $this->single_item = $this->getAdjustedPackage($this->single_item);
            }
            return array_fill(0, $quantity, $this->single_item);
        }
        // Toggle optional constraints based on item packed singly
        $this->updateOptionalConstraints($this->single_item);
        if (!empty($this->merge_strategies)) {
            $quantity = $this->merge($packages, $this->single_item, $quantity);
        }
        // If more than one remain, pack recursively
        if ($quantity > 1) {
            // Update item with converted unit values, remaining quantity, etc.
            $item = array_merge($item, $lwh, array('weight'=>$product_weight, 'quantity'=>$quantity, 'options'=>$options));
            // Reset optional constraint status to that of single item after attempted merging
            $this->updateOptionalConstraints($this->single_item);
            $new_packages = $this->recursivePackageWorker(array($item));
            // Adjust package weight for packaging material
            if ($this->packaging_weight > 0) {
                foreach ($new_packages as $index => $package) {
                    $new_packages[$index] = $this->getAdjustedPackage($package);
                }
            }
            return $new_packages;
        }
        // Adjust package weight for packaging material
        if ($quantity > 0 && $this->packaging_weight > 0) {
            $this->single_item = $this->getAdjustedPackage($this->single_item);
        }
        // Remaining quantity is either 0 or 1; return array filled with that many packages
        return ($quantity > 0 ? array($this->single_item) : array());
    }

    /**
     * Override
     * @return array containing various data:
     *     'manufacturer' => int manufacturer id
     *     'ships_individually' => boolean true if item is flagged to ship individually
     */
    protected function getPackageOptions($item) {
        return array(
                'manufacturer' => (isset($item['manufacturer']) ? filter_var($item['manufacturer'], FILTER_VALIDATE_INT) : null),
                'ships_individually' => !empty($item['ships_individually']),
            );
    }

    /**
     * Construct the additional package from the original item and the additional package data
     * @param array $item The original item array
     * @param array $data The additional package data from the original item
     */
    private function getAdditionalPackage(array $item, array $data) {
        $addon = array();
        // CubeCart prefixes certain columns with 'product_'
        foreach ($data as $key => $value) {
            if (in_array($key, array('weight','width','height','depth'))) {
                $addon['product_'.$key] = $value;
            } else {
                $addon[$key] = $value;
            }
        }
        // Keep any other data from original item after removing unwanted keys
        unset($item['additional_packages'], $item['ignore_main_package'], $item['ships_individually']);
        $addon = array_replace($item, $addon);
        return $addon;
    }

    private function getAdjustedPackage(\Awsp\Ship\Package $package) {
        $weight = $package->get('weight') + $this->packaging_weight;
        $dimensions = array($package->get('length'), $package->get('width'), $package->get('height'));
        return new \Awsp\Ship\Package($weight, $dimensions, $package->get('options'));
    }
}
