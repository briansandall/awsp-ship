<?php
/**
 * Default merging strategy uses a simple vertical stacking algorithm that can result in
 * a lot of wasted space depending on the dimensions and quantity of the packed items.
 *
 * The constructor takes an optional callback that can be used for finer control over the
 * merge strategy without having to write a new implementation.
 *
 * @package Awsp MergeStrategy Package
 * @author Brian Sandall
 * @copyright (c) 2017 Brian Sandall
 * @version 03/22/2017 - NOTICE: This is beta software.  Although it has been tested, there may be bugs and 
 *      there is plenty of room for improvement.  Use at your own risk.
 * @license MIT License http://www.opensource.org/licenses/mit-license.php
 */
namespace Awsp\MergeStrategy;

use \Awsp\Ship\Package as Package;

class DefaultMergeStrategy implements IMergeStrategy
{
    private $callback;

    /**
     * @param callable $callback Optional function that will be called during #merge using the same parameters.
     *                           Return false from this function to prevent the merge.
     */
    public function __construct(callable $callback = null) {
        $this->callback = $callback;
    }

    /**
     * @Override
     */
    public function merge(Package $packageA, Package $packageB, &$error = '') {
        $l = max($packageA->get('length'), $packageB->get('length'));
        $w = max($packageA->get('width'), $packageB->get('width'));
        $h = $packageA->get('height') + $packageB->get('height');
        $weight = $packageA->get('weight') + $packageB->get('weight');
        $combined = new Package($weight, array($l, $w, $h), $packageA->get('options'));
        // Don't forget to merge the package options into the combined package
        if (!$combined->mergeOptions($packageB, $error)) {
            return false;
        } elseif ($this->callback != null && call_user_func_array($this->callback, array($packageA, $packageB, &$error)) === false) {
            return false;
        }
        return $combined;
    }
}
